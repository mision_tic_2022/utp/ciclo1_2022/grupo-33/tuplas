
tupla = ("Juan",)
tupla = "Juan",
miTupla = ("Andres", 2, 3)

print(miTupla[0], miTupla[2])

# No soportado
#miTupla[0] = 'Juan'

nombres = ("Karol", "Roko", "Juan", "Brayan")

n1, n2, n3, n4 = nombres

print(n1)
print(n2)

print('--------------')

tupla_lista = ([1, 2], [3, 4])
print(tupla_lista)

persona_1 = {
    'nombre': 'Juan'
}
persona_2 = {
    'nombre': 'María'
}

tupla_diccionarios = (persona_1, persona_2)

print(tupla_diccionarios)

persona_1['apellido'] = 'Quintero'

print(tupla_diccionarios)
